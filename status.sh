#!/bin/bash

ps aux | grep gateway-0.0.1
id=`echo $(ps aux | grep gateway-0.0.1 | grep -v grep | awk '{print $2}')`
[[ -z "$id" ]] && echo "Application is not running" || echo "Application is running, PID is = " $id
