#!/bin/bash

echo "Collect running pid of gateway-0.0.1 application"
id=`echo $(ps aux | grep gateway-0.0.1 | grep -v grep | awk '{print $2}')`

[[ -z "$id" ]] && echo "Application is not running" || echo "Application PID is = " $id
[[ -z "$id" ]] || echo "Application stopping ..."
[[ -z "$id" ]] || kill -9 $id
