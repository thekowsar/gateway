drop table if exists Peripheraldevice;
drop table if exists Gateway;
/*
List of gateways
oid SELECT * FROM                           : Surrogate primary key
serialNumber                   : A unique serial number (string)
gatewayName                    : Human-readable name (string)
ipv4Address                    : IPv4 address (to be validated)
*/
create table                   Gateway
(
gatewayOid                     varchar(128)                                                not null,
serialNumber                   varchar(16)                                                 not null,
gateWayName                    varchar(128)                                                not null,
ipv4Address                    varchar(16)                                                 not null,
constraint                     pk_gateway                                                  primary key    (gatewayOid),
constraint                     uk_gateway_serial_number			                           unique         (serialNumber)
);

/*
List of peripheral device
oid                            : Surrogate primary key
uid          		           : A unique serial number (number)
vendor                         : Name of vendor (string)
createdOn                      : Creation time
status                         : Status of pheripheral device (online or offline)
gatewayOid                     : Oid of gateway table
*/
create table                   Peripheraldevice
(
peripheralDeviceOid            varchar(128)                                                not null,
uid		                       numeric(24,0)                                                not null,
vendor		                   varchar(256)                                                not null,
createdOn	                   timestamp	                                               not null,
status		                   varchar(8)                   	                           not null,
gatewayOid		               varchar(128)                                       	       not null,
constraint                     pk_peripheral_device                                        primary key    (peripheralDeviceOid),
constraint                     uk_uid					                                   unique         (uid),
constraint                     fk_gateway_oid_peripheral_device                      	   foreign key    (gatewayOid) references     Gateway(gatewayOid),
constraint                     ck_status			                                       check          (status = 'online' or status = 'offline')
);

/*
INSERT INTO Gateway (gatewayOid, serialNumber, gateWayName, ipv4Address) VALUES ('1000001', '020XTQ10C3000017', 'Huawei gateway', '192.168.40.101');
INSERT INTO Gateway (gatewayOid, serialNumber, gateWayName, ipv4Address) VALUES ('1000002', '020YNB10B5000151', 'Actiontec gateway', '192.168.40.101');


INSERT INTO PeripheralDevice (peripheralDeviceOid, uid, vendor, createdOn, status, gatewayOid)
VALUES ('17187f26-283f-44bc-8c06-b600c49e957c', '2000001', 'vendor-x', current_timestamp, 'online', '1000001');
INSERT INTO PeripheralDevice (peripheralDeviceOid, uid, vendor, createdOn, status, gatewayOid)
VALUES ('2000002', '2000002', 'vendor-y', current_timestamp, 'offline', '1000001');
INSERT INTO PeripheralDevice (peripheralDeviceOid, uid, vendor, createdOn, status, gatewayOid)
VALUES ('2000003', '2000003', 'vendor-z', current_timestamp, 'online', '1000001');
*/

