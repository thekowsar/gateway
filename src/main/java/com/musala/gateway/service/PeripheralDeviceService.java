package com.musala.gateway.service;

import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.entity.PeripheralDeviceEntity;
import com.musala.gateway.repository.GatewayRepository;
import com.musala.gateway.repository.PeripheralDeviceRepository;
import com.musala.gateway.request.PeripheralDeviceRequest;
import com.musala.gateway.response.GatewayResponse;
import com.musala.gateway.util.ExceptionHandlerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

import static com.musala.gateway.util.IdGeneratorUtil.generateUniqueId;
import static com.musala.gateway.util.Messages.*;

@Service
@Slf4j
public class PeripheralDeviceService {

    @Autowired
    PeripheralDeviceRepository peripheralDeviceRepository;

    @Autowired
    GatewayRepository gatewayRepository;

    public GatewayResponse deleteByOid(String oid) throws ExceptionHandlerUtil{
        try{
            peripheralDeviceRepository.deleteById(oid);
        } catch (EmptyResultDataAccessException ex) {
            log.error("Successfully deleted peripheral devices by oid: {}", oid);
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, String.format(DEVICE_NOT_EXIST_BY_OID, oid));
        }
        GatewayResponse response = GatewayResponse.builder()
                .obj(oid)
                .userMessage(String.format(DEVICE_DELETED_BY_OID, oid))
                .build();
        return response;
    }

    public GatewayEntity add(PeripheralDeviceRequest request) throws ExceptionHandlerUtil {
        GatewayEntity gatewayEntity = gatewayRepository.findByGatewayOid(request.getGatewayOid());
        if(gatewayEntity == null){
            log.error("Gateway dose not exist with oid: {}", request.getGatewayOid());
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, String.format(GATEWAY_NOT_EXIST_BY_OID, request.getGatewayOid()));
        }
        if(gatewayEntity.getPeripheralDevices().size() >= 10){
            log.error("Already 10 peripheral device added into this gateway: {}", gatewayEntity.getGatewayOid());
            throw new ExceptionHandlerUtil(HttpStatus.NOT_ACCEPTABLE, String.format(GATEWAY_ALREADY_HAVE_10_DEVICE, request.getGatewayOid()));
        }
        try {
            PeripheralDeviceEntity peripheralDeviceEntity = new PeripheralDeviceEntity();
            BeanUtils.copyProperties(request, peripheralDeviceEntity);
            peripheralDeviceEntity.setCreatedOn(new Timestamp(new Date().getTime()));

            peripheralDeviceEntity.setGateway(gatewayEntity);
            peripheralDeviceEntity.setUid(generateUniqueId());
            gatewayEntity.getPeripheralDevices().add(peripheralDeviceEntity);
            gatewayEntity = gatewayRepository.save(gatewayEntity);
        } catch (Exception ex){
          log.error(ex.getMessage(), ex);
          throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
        return gatewayEntity;
    }

}
