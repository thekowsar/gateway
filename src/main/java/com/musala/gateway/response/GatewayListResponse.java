package com.musala.gateway.response;

import com.musala.gateway.entity.GatewayEntity;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GatewayListResponse {

    private List<GatewayEntity> data;
    private Long count;
    private String userMessage;

}
