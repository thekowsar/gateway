package com.musala.gateway.util;

public class Constant {
    public static final String STATUS_OFFLINE = "offline";
    public static final String STATUS_ONLINE = "online";
}
