package com.musala.gateway.service;

import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.repository.GatewayRepository;
import com.musala.gateway.repository.PeripheralDeviceRepository;
import com.musala.gateway.request.GatewayRequest;
import com.musala.gateway.response.GatewayListResponse;
import com.musala.gateway.response.GatewayResponse;
import com.musala.gateway.util.ExceptionHandlerUtil;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static com.musala.gateway.util.Messages.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringBootTest
class GatewayServiceTest {

    @Mock
    GatewayRepository gatewayRepository;

    @InjectMocks
    GatewayService gatewayService;

    public static final String SERIAL_NUMBER = "WES234ER32WDR";
    public static final String GATEWAY_OID = "24324yuy3423u4y";

    public static final GatewayEntity GATEWAY_ENTITY_DUPLICATE = GatewayEntity.builder()
            .gatewayOid(GATEWAY_OID)
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    public static final GatewayEntity GATEWAY_ENTITY_SAVE = GatewayEntity.builder()
            .gatewayOid(GATEWAY_OID)
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    public static final GatewayRequest GATEWAY_REQUEST = GatewayRequest.builder()
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    @Test
    public void checkExistWhenExistReturnFoundMessage() throws ExceptionHandlerUtil {
        when(gatewayRepository.findBySerialNumber(SERIAL_NUMBER)).thenReturn(GATEWAY_ENTITY_DUPLICATE);
        ExceptionHandlerUtil ex = assertThrows(ExceptionHandlerUtil.class, () -> {
            GatewayEntity response = gatewayService.save(GATEWAY_REQUEST);
        });
        assertEquals(String.format(GATEWAY_EXIST_BY_SERIAL_NO, SERIAL_NUMBER), ex.getMessage());
    }

    @Test
    public void saveGatewayWhenSuccessReturnEntity() throws ExceptionHandlerUtil {
        when(gatewayRepository.findBySerialNumber(SERIAL_NUMBER)).thenReturn(null);
        when(gatewayRepository.save(Mockito.any())).thenReturn(GATEWAY_ENTITY_SAVE);
        GatewayEntity response = gatewayService.save(GATEWAY_REQUEST);
        assertNotNull(response);
    }

    @Test
    public void getWhenByOidReturnNotExist() throws ExceptionHandlerUtil {
        when(gatewayRepository.findByGatewayOid(GATEWAY_OID)).thenReturn(null);
        GatewayResponse gatewayResponse = gatewayService.getByOid(GATEWAY_OID);
        assertEquals(String.format(GATEWAY_NOT_EXIST_BY_OID, GATEWAY_OID), gatewayResponse.getUserMessage());
    }

    @Test
    public void getWhenByOidReturnEntity() throws ExceptionHandlerUtil {
        when(gatewayRepository.findByGatewayOid(GATEWAY_OID)).thenReturn(GATEWAY_ENTITY_SAVE);
        GatewayResponse gatewayResponse = gatewayService.getByOid(GATEWAY_OID);
        assertNotNull(gatewayResponse);
        assertEquals(SUCCESSFULLY_GATEWAY_FOUND, gatewayResponse.getUserMessage());
    }

    @Test
    public void getCountWhenZeroReturnNotFound() throws ExceptionHandlerUtil {
        when(gatewayRepository.count()).thenReturn(0L);
        GatewayListResponse response = gatewayService.findAll(0, 10);
        assertEquals(DATA_NOT_FOUNT, response.getUserMessage());
    }

    @Test
    public void getAllWhenListReturnData() throws ExceptionHandlerUtil {
        when(gatewayRepository.count()).thenReturn(1L);
        List<GatewayEntity> dataList = new ArrayList<GatewayEntity>();
        dataList.add(GATEWAY_ENTITY_SAVE);
        Pageable pageable = PageRequest.of(0, 10);
        Page<GatewayEntity> page = new PageImpl<>(dataList, pageable, dataList.size());
        when(gatewayRepository.findAll(pageable)).thenReturn(page);
        GatewayListResponse response = gatewayService.findAll(0, 10);
        assertEquals(SUCCESSFULLY_GATEWAY_LIST_FOUND, response.getUserMessage());
        assertEquals(1, response.getData().size());
    }

}