package com.musala.gateway.service;

import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.entity.PeripheralDeviceEntity;
import com.musala.gateway.repository.GatewayRepository;
import com.musala.gateway.repository.PeripheralDeviceRepository;
import com.musala.gateway.request.PeripheralDeviceRequest;
import com.musala.gateway.response.GatewayResponse;
import com.musala.gateway.util.ExceptionHandlerUtil;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.musala.gateway.util.Messages.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class PeripheralDeviceServiceTest {

    @Mock
    PeripheralDeviceRepository peripheralDeviceRepository;

    @Mock
    GatewayRepository gatewayRepository;

    @InjectMocks
    PeripheralDeviceService peripheralDeviceService;

    public static final String SERIAL_NUMBER = "WES234ER32WDR";
    public static final String GATEWAY_OID = "34ref08r24e324";
    public static final long UUID = 12323456675L;
    public static final String DEVICE_OID = "ewf34gse323fgw3";

    public static final PeripheralDeviceRequest REQUEST = PeripheralDeviceRequest.builder()
            .gatewayOid(GATEWAY_OID)
            .vendor("Test Vendor 1")
            .status("online")
            .build();

    public static final GatewayEntity GATEWAY_ENTITY_SAVE = GatewayEntity.builder()
            .gatewayOid(GATEWAY_OID)
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    @Test
    public void checkGatewayWhenNullReturnNotFound(){
        when(gatewayRepository.findByGatewayOid(GATEWAY_OID)).thenReturn(null);
        ExceptionHandlerUtil ex = assertThrows(ExceptionHandlerUtil.class, () -> {
            peripheralDeviceService.add(REQUEST);
        });
        assertEquals(String.format(GATEWAY_NOT_EXIST_BY_OID, GATEWAY_OID), ex.getMessage());
    }

    @Test
    public void checkGatewayDeviceWhenTenDeviceHaveReturnTenDeviceAdded(){
        Set<PeripheralDeviceEntity> deviceSet = new HashSet<>();
        Timestamp currentTime = new Timestamp(new Date().getTime());
        for(int i = 0; i < 10; i++){
            PeripheralDeviceEntity peripheralDeviceEntity = PeripheralDeviceEntity.builder()
                    .peripheralDeviceOid(String.valueOf(i))
                    .uid(Long.valueOf(i))
                    .vendor("Test Vendor".concat(String.valueOf(i)))
                    .status("Online")
                    .createdOn(currentTime)
                    .build();
            deviceSet.add(peripheralDeviceEntity);
        }
        GATEWAY_ENTITY_SAVE.setPeripheralDevices(deviceSet);
        when(gatewayRepository.findByGatewayOid(GATEWAY_OID)).thenReturn(GATEWAY_ENTITY_SAVE);
        ExceptionHandlerUtil ex = assertThrows(ExceptionHandlerUtil.class, () -> {
            peripheralDeviceService.add(REQUEST);
        });
        assertEquals(String.format(GATEWAY_ALREADY_HAVE_10_DEVICE, GATEWAY_OID), ex.getMessage());
    }

    @Test
    public void saveDeviceWhenSuccessReturnEntity() throws ExceptionHandlerUtil {
        Set<PeripheralDeviceEntity> deviceSet = new HashSet<>();
        GATEWAY_ENTITY_SAVE.setPeripheralDevices(deviceSet);
        when(gatewayRepository.findByGatewayOid(GATEWAY_OID)).thenReturn(GATEWAY_ENTITY_SAVE);
        PeripheralDeviceEntity peripheralDeviceEntity = PeripheralDeviceEntity.builder()
                .peripheralDeviceOid(String.valueOf(UUID))
                .uid(UUID)
                .vendor("Test Vendor 1")
                .status("Online")
                .createdOn(new Timestamp(new Date().getTime()))
                .build();
        deviceSet.add(peripheralDeviceEntity);
        GATEWAY_ENTITY_SAVE.setPeripheralDevices(deviceSet);
        when(gatewayRepository.save(Mockito.any())).thenReturn(GATEWAY_ENTITY_SAVE);
        GatewayEntity gatewayEntity = peripheralDeviceService.add(REQUEST);
        assertNotNull(gatewayEntity);
        assertEquals(GATEWAY_OID, gatewayEntity.getGatewayOid());
    }

    @Test
    public void checkDeviceWhenEmptyReturnDeviceNotFound(){
        doThrow(EmptyResultDataAccessException.class)
                .when(peripheralDeviceRepository)
                .deleteById(DEVICE_OID);
        ExceptionHandlerUtil ex = assertThrows(ExceptionHandlerUtil.class, () -> {
            peripheralDeviceService.deleteByOid(DEVICE_OID);
        });
        assertEquals(String.format(DEVICE_NOT_EXIST_BY_OID, DEVICE_OID), ex.getMessage());
    }

    @Test
    public void deleteDeviceWhenSuccessReturnSuccessMessage() throws ExceptionHandlerUtil {
        doNothing()
                .when(peripheralDeviceRepository)
                .deleteById(DEVICE_OID);
        GatewayResponse response = peripheralDeviceService.deleteByOid(DEVICE_OID);
        assertEquals(String.format(DEVICE_DELETED_BY_OID, DEVICE_OID), response.getUserMessage());
    }

}