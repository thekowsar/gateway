package com.musala.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.request.PeripheralDeviceRequest;
import com.musala.gateway.response.GatewayResponse;
import com.musala.gateway.service.PeripheralDeviceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.musala.gateway.util.Messages.DEVICE_DELETED_BY_OID;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PeripheralDeviceController.class)
class PeripheralDeviceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    PeripheralDeviceService peripheralDeviceService;

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    public static final String SERIAL_NUMBER = "WES234ER32WDR";
    public static final String GATEWAY_OID = "34ref08r24e324";
    public static final long UUID = 12323456675L;
    public static final String DEVICE_OID = "ewf34gse323fgw3";

    public static final PeripheralDeviceRequest REQUEST = PeripheralDeviceRequest.builder()
            .gatewayOid(GATEWAY_OID)
            .vendor("Test Vendor 1")
            .status("online")
            .build();

    public static final GatewayEntity GATEWAY_ENTITY_SAVE = GatewayEntity.builder()
            .gatewayOid(GATEWAY_OID)
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    public static final GatewayResponse DELETE_RESPONSE = GatewayResponse.builder()
            .obj(DEVICE_OID)
            .userMessage(String.format(DEVICE_DELETED_BY_OID, DEVICE_OID))
            .build();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @Test
    public void successfulSaveRequestTest() throws Exception {
        when(peripheralDeviceService.add(Mockito.any())).thenReturn(GATEWAY_ENTITY_SAVE);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/peripheral/device/save")
                .content(objectMapper.writeValueAsString(REQUEST))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

    @Test
    public void successfulDeleteRequestTest() throws Exception {
        when(peripheralDeviceService.deleteByOid(Mockito.any())).thenReturn(DELETE_RESPONSE);
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/v1/peripheral/device/delete/" + DEVICE_OID)
                .content(objectMapper.writeValueAsString(REQUEST))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

}