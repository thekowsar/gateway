package com.musala.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.request.GatewayRequest;
import com.musala.gateway.response.GatewayListResponse;
import com.musala.gateway.response.GatewayResponse;
import com.musala.gateway.service.GatewayService;
import com.musala.gateway.util.ExceptionHandlerUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.musala.gateway.util.Messages.*;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = GatewayController.class)
class GatewayControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GatewayService gatewayService;

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    public static final String GATEWAY_OID = "24324yuy3423u4y";
    public static final String SERIAL_NUMBER_EXIST = "WES234ER32WDRe";
    public static final String SERIAL_NUMBER = "N4ES234ER32WDR";

    GatewayResponse gatewaySuccessResponse = GatewayResponse.builder()
            .obj(new GatewayEntity())
            .userMessage(SUCCESSFULLY_GATEWAY_FOUND)
            .build();

    GatewayResponse gatewayNotFoundResponse = GatewayResponse.builder()
            .obj(new GatewayEntity())
            .userMessage(String.format(GATEWAY_NOT_EXIST_BY_OID, GATEWAY_OID))
            .build();
    GatewayListResponse gatewayListResponse =GatewayListResponse.builder()
            .userMessage(SUCCESSFULLY_GATEWAY_LIST_FOUND)
            .build();

    public static final GatewayEntity GATEWAY_ENTITY_SAVE = GatewayEntity.builder()
            .gatewayOid(GATEWAY_OID)
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    public static final GatewayRequest GATEWAY_REQUEST = GatewayRequest.builder()
            .serialNumber(SERIAL_NUMBER)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    public static final GatewayRequest GATEWAY_REQUEST_EXIST = GatewayRequest.builder()
            .serialNumber(SERIAL_NUMBER_EXIST)
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @Test
    public void whenSerialNumberDuplicateShouldSendFound() throws Exception {
        when(gatewayService.save(ArgumentMatchers.refEq(GATEWAY_REQUEST_EXIST, "serialNumber"))).thenThrow(new ExceptionHandlerUtil(HttpStatus.FOUND, String.format(GATEWAY_EXIST_BY_SERIAL_NO, GATEWAY_REQUEST.getSerialNumber())));
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/gateway/save")
                .content(objectMapper.writeValueAsString(GATEWAY_REQUEST_EXIST))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(CONTENT_TYPE))
                .andExpect(status().isFound());
    }

    @Test
    public void successfulSaveRequestTest() throws Exception {
        when(gatewayService.save(ArgumentMatchers.refEq(GATEWAY_REQUEST, "serialNumber"))).thenReturn(GATEWAY_ENTITY_SAVE);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/v1/gateway/save")
                .content(objectMapper.writeValueAsString(GATEWAY_REQUEST))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

    @Test
    public void chechGetByOidWhenValidRequestShouldGetSuccessCode() throws Exception{
        when(gatewayService.getByOid(GATEWAY_OID)).thenReturn(gatewaySuccessResponse);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/gateway/get-by-oid/" + GATEWAY_OID)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists());
    }

    @Test
    public void get404ResponseWhenNoDataFoundWithGivenSearchTextTest() throws Exception{
        when(gatewayService.getByOid(GATEWAY_OID)).thenReturn(gatewayNotFoundResponse);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/v1/gateway/get-by-oid/" + GATEWAY_OID)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.userMessage", is(String.format(GATEWAY_NOT_EXIST_BY_OID, GATEWAY_OID))));
    }

    @Test
    public void getNotFoundResponseWhenListZero() throws Exception{
        when(gatewayService.findAll(0, 10)).thenReturn(gatewayListResponse);
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/gateway/get-all?offset=0&limit=10")
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userMessage", is(SUCCESSFULLY_GATEWAY_LIST_FOUND)));
    }

}