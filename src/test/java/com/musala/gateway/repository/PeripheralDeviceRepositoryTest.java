package com.musala.gateway.repository;

import com.musala.gateway.entity.GatewayEntity;
import com.musala.gateway.entity.PeripheralDeviceEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class PeripheralDeviceRepositoryTest {

    @Autowired
    GatewayRepository gatewayRepository;

    @Autowired
    PeripheralDeviceRepository peripheralDeviceRepository;

    private static final GatewayEntity gatewayEntity_1 = GatewayEntity.builder()
            .serialNumber("ED3221W32131Q")
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    private static final GatewayEntity gatewayEntity_2 = GatewayEntity.builder()
            .serialNumber("ED3221W321322")
            .gatewayName("Test Gateway 2")
            .ipv4Address("192.168.0.2")
            .build();

    private static final PeripheralDeviceEntity peripheralDeviceEntity = PeripheralDeviceEntity.builder()
            .uid(29382938283L)
            .createdOn(new Timestamp(new Date().getTime()))
            .vendor("Test Vendor 1")
            .status("online")
            .build();

    @Test
    void injectedComponentsAreNotNull(){
        assertNotNull(peripheralDeviceRepository);
    }

    @Test
    void whenDeviceSavedThenSavedEntityShouldNotNull(){
        GatewayEntity savedEntity = gatewayRepository.save(gatewayEntity_1);
        peripheralDeviceEntity.setGateway(savedEntity);
        PeripheralDeviceEntity savedDeviceEntity = peripheralDeviceRepository.save(peripheralDeviceEntity);
        assertNotNull(savedDeviceEntity);
        assertEquals(savedDeviceEntity.getUid(), peripheralDeviceEntity.getUid());
    }

    @Test
    void whenDeviceDeletedThenDeviceEntityShouldBeNull(){
        GatewayEntity savedEntity = gatewayRepository.save(gatewayEntity_1);
        peripheralDeviceEntity.setGateway(savedEntity);
        PeripheralDeviceEntity savedDeviceEntity = peripheralDeviceRepository.save(peripheralDeviceEntity);
        peripheralDeviceRepository.deleteById(savedDeviceEntity.getPeripheralDeviceOid());
        PeripheralDeviceEntity deletedDevice = peripheralDeviceRepository.findByPeripheralDeviceOid(savedDeviceEntity.getPeripheralDeviceOid());
        assertNull(deletedDevice);
    }

}