package com.musala.gateway.repository;

import com.musala.gateway.entity.GatewayEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
class GatewayRepositoryTest {

    @Autowired
    GatewayRepository gatewayRepository;

    private static final GatewayEntity gatewayEntity_1 = GatewayEntity.builder()
            .serialNumber("ED3221W32131Q")
            .gatewayName("Test Gateway 1")
            .ipv4Address("192.168.0.1")
            .build();

    private static final GatewayEntity gatewayEntity_2 = GatewayEntity.builder()
            .serialNumber("ED3221W321322")
            .gatewayName("Test Gateway 2")
            .ipv4Address("192.168.0.2")
            .build();

    @Test
    void injectedComponentsAreNotNull(){
        assertNotNull(gatewayRepository);
    }

    @Test
    void whenGateWaySavedThenGatewayEntityNotNull(){
        GatewayEntity savedEntity = gatewayRepository.save(gatewayEntity_1);
        assertNotNull(savedEntity);
        assertEquals(savedEntity.getSerialNumber(), gatewayEntity_1.getSerialNumber());
    }

    @Test
    void whenNotSavedThenFindByOidShouldBeNull(){
        GatewayEntity findEntity = gatewayRepository.findByGatewayOid("test-0id");
        assertNull(findEntity);
    }

    @Test
    void whenSavedThenFindByOid(){
        GatewayEntity savedEntity = gatewayRepository.save(gatewayEntity_1);
        GatewayEntity findEntity = gatewayRepository.findByGatewayOid(savedEntity.getGatewayOid());
        assertNotNull(findEntity);
    }

    @Test
    void whenSavedThenFindByOidAndCheckGatewayName(){
        GatewayEntity savedEntity = gatewayRepository.save(gatewayEntity_1);
        GatewayEntity findEntity = gatewayRepository.findByGatewayOid(savedEntity.getGatewayOid());
        assertNotNull(findEntity);
        assertEquals(savedEntity.getGatewayName(), findEntity.getGatewayName());
    }

    @Test
    void whenGetAllThenSizeShouldNotZero(){
        gatewayRepository.save(gatewayEntity_1);
        gatewayRepository.save(gatewayEntity_2);
        Pageable pageable = PageRequest.of(0, 10);
        Page<GatewayEntity> page = gatewayRepository.findAll(pageable);
        assertNotNull(page);
        assertNotNull(page.getContent());
    }

    @Test
    void whenSavedTwoThenCountShouldBeTwo(){
        gatewayRepository.save(gatewayEntity_1);
        gatewayRepository.save(gatewayEntity_2);
        assertEquals(gatewayRepository.count(), 2);
    }

}