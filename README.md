# Gateway

Spring boot project for Musala soft with Maven and JDK 8. This application used to store Gateway information.


## Used Technology

- **Programming language**: Java
- **Framework**: Spring Boot
- **Database**: In-memory (H2 DB)
- **Automated build**: Apache Maven

## Prerequisites

- **JDK 8**
- **Maven**

## How To Run

1. Go to the project root directory.
2. Open terminal in this project root directory.
3. Give executable permission to start.sh, status.sh and stop.sh.
- `chmod a+x start.sh `
- `chmod a+x stop.sh `
- `chmod a+x status.sh `
4. Run script to run application.
- `./start.sh`
5. Project will run in **http://127.0.0.1:9090/**
6. Here added Postmant script **postman-collection.json**  to test services.

## How To Connect H2 DB
After running the application do as follow:
1. Pest this url **http://localhost:9090/h2-console** in browser.
2. You will get login window.
3. Pest **jdbc:h2:mem:testdb** in **JDBC URL:** input field.
4. Pest **sa** in **User Name:** input field.
5. Leave blank **Password:** input field.
6. Click **Connect** button.

## API Doc

#### 1. Save Gateway
**Http Method:** POST
**URL:** http://127.0.0.1:9090/v1/gateway/save
**Request Body:**
```
{
    "serialNumber": "020XTQ10C300012",
    "gatewayName": "test-gateway-01",
    "ipv4Address": "192.168.40.1"
}
```
| field | data type |description|
| ------ | ------ | ------ |
| serialNumber | String | A unique serial number |
| gatewayName | String | Human-readable name |
| ipv4Address | String | IPv4 address |

**Response Body:**
```
{
    "gatewayOid": "bda90f32-00eb-49f6-b8ce-3432e0681a69",
    "serialNumber": "020XTQ10C300012",
    "gatewayName": "test-gateway-01",
    "ipv4Address": "192.168.40.1",
    "peripheralDevices": null
}
```


**CURL Command:**
```
curl --location --request POST 'http://127.0.0.1:9090/v1/gateway/save' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serialNumber": "020XTQ10C300012",
    "gatewayName": "test-gateway-01",
    "ipv4Address": "192.168.40.1"
}'
```


#### 2. Get Gateway List
**Http Method:** GET
**URL:** http://127.0.0.1:9090/v1/gateway/get-all?offset=0&limit=10

| field | data type |description|
| ------ | ------ | ------ |
| offset | Integer | page number |
| limit | Integer | Number of data to be shown |

** Response Body **
```
{
    "data": [
        {
            "gatewayOid": "bda90f32-00eb-49f6-b8ce-3432e0681a69",
            "serialNumber": "020XTQ10C300012",
            "gatewayName": "test-gateway-01",
            "ipv4Address": "192.168.40.1",
            "peripheralDevices": [
                {
                    "peripheralDeviceOid": "3e428fd3-ed35-4e3e-80fc-109ae684b2c2",
                    "uid": 5326764216936317416,
                    "vendor": "vendor 1",
                    "createdOn": "2021-04-24T20:44:33.605+00:00",
                    "status": "online"
                }
            ]
        }
    ],
    "count": 1,
    "userMessage": "Successfully get gateway list"
}
```


**CURL Command:**
`curl --location --request GET 'http://127.0.0.1:9090/v1/gateway/get-all?offset=0&limit=10'`

#### 3. Get Single Gateway Details
**Http Method:** GET
**URL:** http://127.0.0.1:9090/v1/gateway/get-by-oid/{oid}

| field | data type |description|
| ------ | ------ | ------ |
| oid | String | Unique id of Gateway |

**Response Body:**
```
{
    "obj": {
        "gatewayOid": "bda90f32-00eb-49f6-b8ce-3432e0681a69",
        "serialNumber": "020XTQ10C300011",
        "gatewayName": "020XTQ10C300012",
        "ipv4Address": "192.168.40.1",
        "peripheralDevices": [
            {
                "peripheralDeviceOid": "3e428fd3-ed35-4e3e-80fc-109ae684b2c2",
                "uid": 5326764216936317416,
                "vendor": "vendor 1",
                "createdOn": "2021-04-24T20:44:33.605+00:00",
                "status": "online"
            }
        ]
    },
    "userMessage": "Successfully get gateway",
    "peripheralDevices": [
        {
            "peripheralDeviceOid": "40229544-14ae-4b14-b041-b1b8a822b6b3",
            "uid": 3922925305815777728,
            "vendor": "vendor 1",
            "createdOn": "2021-04-24T20:48:47.289+00:00",
            "status": "online"
        }
    ]
}
```


**CURL Command:**
`curl --location --request GET 'http://127.0.0.1:9090/v1/gateway/get-by-oid/a5cdb289-4cc6-41b8-8f05-36ad1ad2eae6'`

#### 4. Add Peripharal Device To A Gateway
**Http Method:** POST
**URL:** http://127.0.0.1:9090/v1/peripheral/device/save
**Request Body:**
```
{
    "vendor": "vendor 1",
    "status": "online",
    "gatewayOid": "a5cdb289-4cc6-41b8-8f05-36ad1ad2eae6"
}
```

| field | data type |description|
| ------ | ------ | ------ |
| vendor | Integer | vendor name |
| status | String | online/offline |
| gatewayOid | String | Unique id of Gateway |

**Response Body:**
```
{
    "gatewayOid": "a5cdb289-4cc6-41b8-8f05-36ad1ad2eae6",
    "serialNumber": "020XTQ10C300011",
    "gatewayName": "test-uuid-13",
    "ipv4Address": "192.168.40.1",
    "peripheralDevices": [
        {
            "peripheralDeviceOid": "3e428fd3-ed35-4e3e-80fc-109ae684b2c2",
            "uid": 5326764216936317416,
            "vendor": "vendor 1",
            "createdOn": "2021-04-24T20:44:33.605+00:00",
            "status": "online"
        }
    ]
}
```

**CURL Command**
```
curl --location --request POST 'http://127.0.0.1:9090/v1/peripheral/device/save' \
--header 'Content-Type: application/json' \
--data-raw '{
    "vendor": "test-uuid-12",
    "status": "offline",
    "gatewayOid": "9bf39d6e-76c9-46ac-82d0-42127d3113dd"
}'
```



#### 5. Remove Peripharal Device From A Gateway
**Http Method:** DELETE
**URL:** http://127.0.0.1:9090/v1/peripheral/device/delete/{oid}

| field | data type |description|
| ------ | ------ | ------ |
| oid | String | Unique Id of peripheral device |

**Response Body:**
```
{
    "obj": "3e428fd3-ed35-4e3e-80fc-109ae684b2c2",
    "userMessage": "Successfully peripheral device deleted by oid: 3e428fd3-ed35-4e3e-80fc-109ae684b2c2"
}
```

**CURL Command**
`curl --location --request DELETE 'http://127.0.0.1:9090/v1/peripheral/device/delete/3e428fd3-ed35-4e3e-80fc-109ae684b2c2'`
